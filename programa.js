function cargaMapa() {
    let mapa = L.map('divMapa', { center: [4.60971, -74.08175], zoom: 16});

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });

    mosaico.addTo(mapa);

    let barrio = L.polygon([
        [4.645295025120784,-74.16071891784668],
        [4.653293787750683,-74.16973114013672],
        [4.647519289083772,-74.18076038360596],
        [4.628741148132516,-74.17084693908691],
        [4.640760926856299,-74.15677070617676],
        [4.645295025120784,-74.16071891784668]
    ]).addTo(mapa);


    let markerBogota = L.marker([4.633055556, -74.16472222]);
    markerBogota.addTo(mapa);

}
